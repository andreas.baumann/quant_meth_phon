library(MuMIn)
library(mgcv)
library(brms)
library(jtools)

library(magrittr)
library(dplyr)
library(ggplot2)
library(rstan)
library(tidybayes)
library(modelr)

# get tables
tokens = read.csv("data/tokens.csv")
types = read.csv("data/types.csv")
phonemes = read.delim("data/phonemes.csv")
manners = read.delim("data/manners.csv")
places = read.delim("data/places.csv")

# add moa/poa info to phonemes
phonemes = subset(phonemes, vowel_consonant == "C")
phonemes = merge(phonemes, manners, "manner")
phonemes = merge(phonemes, places, "place")
phonemes = subset(phonemes, select = c(phoneme, moa_score, poa_score, phonation))

# add moa/poa to diphones
types = merge(types, phonemes, by.x = "const1", by.y = "phoneme")
types = merge(types, phonemes, by.x = "const2", by.y = "phoneme")
types = subset(types, select = c(diphone, moa_score.x, poa_score.x, moa_score.y, poa_score.y, phonation.x, phonation.y))

# delta moa/poa
types$delta_moa = abs(types$moa_score.x - types$moa_score.y)
types$delta_poa = abs(types$poa_score.x - types$poa_score.y)
types$delta_phonation = as.numeric(types$phonation.x != types$phonation.y)

# internal diphones only
frequencies = table(tokens$diphone[tokens$position!="ext"])   #[tokens$position=="ext",]
frequencies = as.data.frame(frequencies)
colnames(frequencies) = c("diphone", "frequency")

df_int = merge(types, frequencies, by = "diphone")[,-1]

# all diphones (including exclusively external ones)
frequencies = table(tokens$diphone)   #[tokens$position=="ext",]
frequencies = as.data.frame(frequencies)
colnames(frequencies) = c("diphone", "frequency")

df_all = merge(types, frequencies, by = "diphone")[,-1]




#### mumin ####

plot_rvi = function(confset, col){
  vars = names(sw(confset))
  rvi = as.vector(sw(confset))
  barplot(rvi, col = col, names.arg = vars, ylab = "relative variable importance", las = 2)
}

options(na.action = "na.fail") 

# internal model 

mdl= lm(log(frequency) ~ ., data = df_int)
summary(mdl)

dredgetab = dredge(mdl, rank="AICc")
confset_int = dredgetab #subset(dredgetab, cumsum(dredgetab$weight) <= .95)

average.mdl = model.avg(confset_int)
summary(average.mdl)


# all diphones model 

mdl = lm(log(frequency) ~ ., data = df_all)
summary(mdl)

dredgetab = dredge(mdl, rank="AICc")
confset_all = dredgetab #subset(dredgetab, cumsum(dredgetab$weight) <= .95)

average.mdl = model.avg(confset_all)
summary(average.mdl)

# compare int vs. all
par(mar = c(8, 4, 4, 4), mfrow = c(1,2))

plot_rvi(confset_int, col = "orange")
plot_rvi(confset_all, col = "darkseagreen")




#### gams ####

# change preprocessing

# delta moa/poa
types$diff_moa = (types$moa_score.x - types$moa_score.y)

# internal diphones only
frequencies = table(tokens$diphone[tokens$position!="ext"])   #[tokens$position=="ext",]
frequencies = as.data.frame(frequencies)
colnames(frequencies) = c("diphone", "frequency")

df_int = merge(types, frequencies, by = "diphone")[,-1]

# all diphones (including exclusively external ones)
frequencies = table(tokens$diphone)   #[tokens$position=="ext",]
frequencies = as.data.frame(frequencies)
colnames(frequencies) = c("diphone", "frequency")

df_all = merge(types, frequencies, by = "diphone")[,-1]




mdl = gam(frequency ~ te(diff_moa, k = 3) + delta_phonation, data = df_int, family = poisson())
plot.gam(mdl, select = 1, col = "orange")

#plot(mdl, n2 = 20, n3 = 3, scheme = 2, hcolors = hcl.colors(70, palette = "OrRd"))


mdl = gam(frequency ~ s(diff_moa, k = 3), data = df_all, family = poisson())
summary(mdl)
plot.gam(mdl, select = 1, col = "darkseagreen")



#### bayesian ####

# http://mjskay.github.io/tidybayes/
# https://cran.r-project.org/web/packages/tidybayes/vignettes/tidy-brms.html


# internal
mdl_int = brm(bf(frequency ~ s(diff_moa, k = 3)),
    data = df_int, family = poisson(link = "log"), cores = 2, seed = 17,
    iter = 4000, warmup = 1000, thin = 10, refresh = 0,
    control = list(adapt_delta = 0.99))


# all
mdl_all = brm(bf(frequency ~ s(diff_moa, k = 3)),
          data = df_all, family = poisson(link = "log"), cores = 2, seed = 17,
          iter = 4000, warmup = 1000, thin = 10, refresh = 0,
          control = list(adapt_delta = 0.99))



df_int %>%
  data_grid(diff_moa = seq_range(diff_moa, n = 100)) %>%
  add_predicted_draws(mdl_int) %>%
  ggplot(aes(x = diff_moa, y = frequency)) +
  stat_lineribbon(aes(y = .prediction), .width = c(.99, .95, .8, .5), color = "brown") +
  #geom_point(data = df_int, size = 2, col = "#80808030") +
  theme_classic() +
  ylim(0, 1250) +
  scale_fill_brewer(palette = "Oranges")

df_all %>%
  data_grid(diff_moa = seq_range(diff_moa, n = 100)) %>%
  add_predicted_draws(mdl_all) %>%
  ggplot(aes(x = diff_moa, y = frequency)) +
  stat_lineribbon(aes(y = .prediction), .width = c(.99, .95, .8, .5), color = "darkgreen") +
  #geom_point(data = df_int, size = 2, col = "#80808030") +
  theme_classic() + 
  ylim(0, 1250) +
  scale_fill_brewer(palette = "BuGn")



df_all %>%
  data_grid(diff_moa = seq_range(diff_moa, n = 100)) %>%
  # NOTE: this shows the use of ndraws to subsample within add_epred_draws()
  # ONLY do this IF you are planning to make spaghetti plots, etc.
  # NEVER subsample to a small sample to plot intervals, densities, etc.
  add_epred_draws(mdl_all, ndraws = 100) %>%   # sample 100 means from the posterior
  ggplot(aes(x = diff_moa, y = frequency)) +
  geom_line(aes(y = .epred, group = .draw), alpha = 0.2, color = "darkseagreen") +
  theme_classic() + 
  ylim(0, 1000) +
  geom_point(data = df_int, size = 2, col = "#80808030")


prior_summary(mdl_int)
